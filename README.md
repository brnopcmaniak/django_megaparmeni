Django-Megaparmeni
==================

Websites for server Megaparmeni.eu

Neededs:
--------
  * DjangoBB - https://bitbucket.org/slav0nic/djangobb
  * Django Social Auth - https://github.com/omab/django-social-auth
  * python oauth2 - https://github.com/simplegeo/python-oauth2
  * python openid - https://pypi.python.org/pypi/python-openid/

Copyrights and Licence
----------------------



- django-social-auth:
    
        Copyright (c) 2010-2012, Matías Aguirre
        you can find on https://github.com/omab/django-social-auth/  
    
    Some bits were derived from others' work and copyrighted by:
    
    - django-twitter-oauth:

            Original Copyright goes to Henrik Lied (henriklied)
            Code borrowed from https://github.com/henriklied/django-twitter-oauth

    - django-openid-auth:

            django-openid-auth -  OpenID integration for django.contrib.auth
            Copyright (C) 2007 Simon Willison
            Copyright (C) 2008-2010 Canonical Ltd.
- djangoBB:

        Copyright (c) 2008-2009, Grigoriy Petukhov
        you can find on https://bitbucket.org/slav0nic/djangobb
    - djangoDB
    
            Copyright (c) 2009, Alexey Afinogenov, Maranchuk Sergey
